# Valdash587

Data model Design - BOD

## Instructions 

### TO VIEW
1. https://gitlab.com/aksh2000/valdash587/-/blob/master/VALDASH587.png

### TO EDIT
1. Clone the project and Open in vsCode
2. Open ```VALDASH587.dawio``` file
2. Install ```hediet.vscode-drawio``` extension from market place
3. Select ```min``` as theme [Bottom Left]
